const live = require('../../static/matches_live.json')
const next = require('../../static/matches_next.json')

module.exports = (req, res) => {
  res.status(200).json({
    live,
    next
  })
}