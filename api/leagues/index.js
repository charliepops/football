const leagues = require('express').Router()
const all = require('./all')

leagues.get('/', all)

module.exports = leagues