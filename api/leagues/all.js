const data = require('../../static/leagues.json')

module.exports = (req, res) => {
  res.status(200).json(data)
}