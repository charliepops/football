const routes = require('express').Router()
const leagues = require('./leagues')
const matches = require('./matches')

routes.use('/leagues', leagues)
routes.use('/matches', matches)

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'API!' })
})

module.exports = routes