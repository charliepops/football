export default ({leagues}) => (
  <div className="pure-u-1-3">
    <h2>Leagues</h2>
    {leagues && leagues.length && leagues.map((l)=>(
      <div key={l.id.toString()}>{l.name}</div>
    ))}
  </div>
  
)