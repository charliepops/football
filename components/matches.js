export default ({title, matches}) => {
  const {live, next} = matches
  
  return (
  <div className="pure-u-1-3">
    <h2>Matches</h2>
    {live && <h3>Live</h3>}
    {live && live.length && live.map((m) => (
      <div key={m.id.toString()} style={{borderBottom: '1px solid', margin: '10px'}}>
        {m.localteam_name} {m.localteam_score} <br/>
        {m.visitorteam_name} {m.visitorteam_score}
      </div>
    ))}
    {next && <h3>Coming</h3>}
    {next && next.length && next.map((m) => (
      <div key={m.id.toString()} style={{borderBottom: '1px solid', margin: '10px'}}>
        {m.localteam_name}<br/>
        {m.visitorteam_name}
      </div>
    ))}
  </div>
)}