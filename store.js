import { createStore } from 'redux'

const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'LEAGUE':
            return {...state, league: action.id}
        default:
            return state
    }
}

export const makeStore = (initialState, options) => {
    return createStore(reducer, initialState)
}