import react, { Component } from 'react';
import withRedux from 'next-redux-wrapper';
import fetch from 'isomorphic-unfetch';
import Router from 'next/router';
import Layout from '../components/layout';
import Leagues from '../components/leagues';
import Matches from '../components/matches';
import Details from '../components/details';
import { makeStore } from '../store';

class Index extends Component {
  static async getInitialProps({ store }) {
    const leaguesFetch = await fetch('http://localhost:3000/api/leagues');
    const leagues = await leaguesFetch.json();
    const matchesFetch = await fetch('http://localhost:3000/api/matches');
    const matches = await matchesFetch.json();
    return { leagues, matches };
  }

  constructor(props) {
    super(props);
    this.onLeagueSelect = this.onLeagueSelect.bind(this);
  }

  onLeagueSelect(id) {
    const href = `league=${id}`;
    const as = `league/${id}`;
    Router.push(href, as, { shallow: true });
    store.dispatch({ type: 'LEAGUE', id });
  }

  render() {
    const { leagues, matches } = this.props;
    return (
      <Layout>
        <div className="pure-g">
          <Leagues leagues={leagues} onClick={this.onLeagueSelect} />
          <Matches matches={matches} />
          <Details />
        </div>
      </Layout>
    );
  }
}

Index = withRedux(makeStore, state => state)(Index);

export default Index;
